package movierental;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;

public class CustomerTest {

    @Test
    public void rawReport() {
        Customer customer = customer();

        String expected =
                """
                        Rental Record for Bob
                        	Jaws	2.0
                        	Golden Eye	3.5
                        	Short New	3.0
                        	Long New	6.0
                        	Bambi	1.5
                        	Toy Story	3.0
                        Amount owed is 19.0
                        You earned 7 frequent renter points\
                        """;

        assertEquals(expected, customer.statement());
    }

    @Test
    @Ignore
    public void htmlReport() {
        Customer customer = customer();

        String expectedHtml =
                """
                <h1>Rental Record for <em>Bob</em></h1>
                <table>
                	<tr><td>Jaws</td><td>2.0</td></tr>
                	<tr><td>Golden Eye</td><td>3.5</td></tr>
                	<tr><td>Short New</td><td>3.0</td></tr>
                	<tr><td>Long New</td><td>6.0</td></tr>
                	<tr><td>Bambi</td><td>1.5</td></tr>
                	<tr><td>Toy Story</td><td>3.0</td></tr>
                </table>
                <p>Amount owed is <em>19.0</em></p>
                <p>You earned 7 frequent renter points</p>\
                """;
    }

    private static Customer customer() {
        Customer customer = new Customer("Bob");
        customer.addRental(new Rental(new Movie("Jaws", Movie.REGULAR), 2));
        customer.addRental(new Rental(new Movie("Golden Eye", Movie.REGULAR), 3));
        customer.addRental(new Rental(new Movie("Short New", Movie.NEW_RELEASE), 1));
        customer.addRental(new Rental(new Movie("Long New", Movie.NEW_RELEASE), 2));
        customer.addRental(new Rental(new Movie("Bambi", Movie.CHILDRENS), 3));
        customer.addRental(new Rental(new Movie("Toy Story", Movie.CHILDRENS), 4));
        return customer;
    }
}